﻿Public Class Die
    Public Function ThrowDice() As Integer
        Dim rng As New Random(DateTime.UtcNow.GetHashCode)
        Return rng.Next(1, 7)
    End Function
End Class
