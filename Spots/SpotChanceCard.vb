﻿Public Class SpotChanceCard
    Inherits Place
    Public Function TopDeckChanceCard(ByRef deckOfCards As DeckChance) As ChanceCard
        Dim rng As New Random()
        Return deckOfCards._theDeck.ElementAt(rng.Next(0, deckOfCards._theDeck.Count - 1))
    End Function

    Public Sub New(ByVal name As String, ByVal boardLocation As Integer)
        MyBase._name = name
        MyBase._boardLocation = boardLocation
    End Sub

End Class
