﻿Public Class SpotUtility
    Inherits Place
    Public _price As Integer
    Public _ownedBy As Player

    Public Sub New(ByVal name As String, ByVal price As Integer, ByVal boardLocation As Integer)
        MyBase._name = name
        MyBase._boardLocation = boardLocation
        _price = price
    End Sub

    Public Sub SetUtilityOwnership(ByRef player As Player)
        _ownedBy = player
    End Sub

    Public ReadOnly Property IsOwned
        Get
            If IsNothing(_ownedBy) Then
                Return False
            Else
                Return True
            End If
        End Get
    End Property

    Public ReadOnly Property Price
        Get
            Return _price
        End Get
    End Property
End Class
