﻿
Public Class TheProperty
    Inherits Place

#Region "Variables"
    Public Enum ColorSet
        Brown
        LightBlue
        Maroon
        Orange
        Red
        Yellow
        Green
        Blue
    End Enum

    Private _price As Integer
    Private _rent As Integer
    Private _mortgageValue As Integer
    Private _buildingCost As Integer
    Private _rent_1House As Integer
    Private _rent_2Houses As Integer
    Private _rent_3Houses As Integer
    Private _rent_4Houses As Integer
    Private _rent_Hotel As Integer
    Private _housesOwned As Integer
    Private _hotelsOwned As Integer
    Private _ownedBy As Player
    Private _color As ColorSet

    Public _partOfCompleteSet As Boolean
#End Region

#Region "Properties"
    Public Property Owner As Player
        Get
            Return Me._ownedBy
        End Get
        Set(value As Player)
            Me._ownedBy = value
        End Set
    End Property
    Public Property Price As Integer
        Get
            Return _price
        End Get
        Set(value As Integer)
            Me._price = value
        End Set
    End Property
    Public ReadOnly Property Rent As Integer
        Get
            Return _rent
        End Get
    End Property
    Public ReadOnly Property MortgageValue As Integer
        Get
            Return Me._mortgageValue
        End Get
    End Property
    Public ReadOnly Property BuildingCost As Integer
        Get
            Return Me._buildingCost
        End Get
    End Property
    Public ReadOnly Property RentCost_1House As Integer
        Get
            Return Me._rent_1House
        End Get
    End Property
    Public ReadOnly Property RentCost_2Houses As Integer
        Get
            Return Me._rent_2Houses
        End Get
    End Property
    Public ReadOnly Property RentCost_3Houses As Integer
        Get
            Return Me._rent_3Houses
        End Get
    End Property
    Public ReadOnly Property RentCost_4Houses As Integer
        Get
            Return Me._rent_4Houses
        End Get
    End Property
    Public ReadOnly Property RentCost_Hotel As Integer
        Get
            Return Me._rent_Hotel
        End Get
    End Property
    Public ReadOnly Property Color As ColorSet
        Get
            Return Me._color
        End Get
    End Property
    Public ReadOnly Property IsOwned As Boolean
        Get
            If IsNothing(_ownedBy) Then
                Return False
            Else
                Return True
            End If
        End Get
    End Property
#End Region

#Region "Constructors"
    Public Sub New(ByVal Name As String,
                   ByVal Color As TheProperty.ColorSet,
                   ByVal Price As Integer,
                   ByVal MortgageValue As Integer,
                   ByVal BuildingCost As Integer,
                   ByVal Rent As Integer,
                   ByVal Rent_1House As Integer,
                   ByVal Rent_2House As Integer,
                   ByVal Rent_3House As Integer,
                   ByVal Rent_4House As Integer,
                   ByVal Rent_Hotel As Integer,
                   ByVal BoardLocation As Integer)

        MyBase._name = Name
        Me._color = Color
        Me._price = Price
        Me._mortgageValue = MortgageValue
        Me._buildingCost = BuildingCost
        Me._rent = Rent
        Me._rent_1House = Rent_1House
        Me._rent_2Houses = Rent_2House
        Me._rent_3Houses = Rent_3House
        Me._rent_4Houses = Rent_4House
        Me._rent_Hotel = Rent_Hotel

        Me._housesOwned = 0
        Me._hotelsOwned = 0
        MyBase._boardLocation = BoardLocation
    End Sub
#End Region

#Region "Methods"
    Public Function IncreaseHouseCount() As Boolean
        If _housesOwned < 4 Then
            Me._housesOwned += 1
            Return True
        Else
            Return False
        End If
    End Function

    Public Function IncreaseHotelCount() As Boolean
        If _housesOwned = 4 And Me._hotelsOwned > 0 Then
            Me._hotelsOwned = 1
            Return True
        Else
            Return False
        End If
    End Function

    Public Function GetPropertyColor(ByVal Color As String) As TheProperty.ColorSet
        Dim toReturn As TheProperty.ColorSet
        If Color = "Brown" Then
            toReturn = TheProperty.ColorSet.Brown
        ElseIf Color = "Light Blue" Then
            toReturn = TheProperty.ColorSet.LightBlue
        ElseIf Color = "Maroon" Then
            toReturn = TheProperty.ColorSet.Maroon
        ElseIf Color = "Orange" Then
            toReturn = TheProperty.ColorSet.Orange
        ElseIf Color = "Red" Then
            toReturn = TheProperty.ColorSet.Red
        ElseIf Color = "Yellow" Then
            toReturn = TheProperty.ColorSet.Yellow
        ElseIf Color = "Green" Then
            toReturn = TheProperty.ColorSet.Green
        ElseIf Color = "Blue" Then
            toReturn = TheProperty.ColorSet.Blue
        Else
            toReturn = Nothing
            Throw New Exception("Error during color assignment in GetPropertyColor().")
        End If
        Return toReturn
    End Function
#End Region

End Class
