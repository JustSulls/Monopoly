﻿Public Class Player
    Public Enum Shape
        MazdaRX8
        MitsubishiLancer
        SubaruWRX
    End Enum

    Public _shape As Shape
    Public _money As Integer
    Public _property As List(Of TheProperty)
    Public _railroads As List(Of SpotRailroad)
    Public _utilities As List(Of SpotUtility)
    Public _position As Integer
    Public _boardPlayerOn As Board
    Public _uniqueID As Integer
    Public _hasGetOutOfJailCard As Boolean
    Public _currentlyInJail As Boolean

    Public _completeBrown As Boolean
    Public _completeLightBlue As Boolean
    Public _completeMaroon As Boolean
    Public _completeOrange As Boolean
    Public _completeRed As Boolean
    Public _completeYellow As Boolean
    Public _completeGreen As Boolean
    Public _completeBlue As Boolean

    Private _listOfCompleteSets As List(Of Boolean)

    Private _die As Die

    Public Sub New(ByVal shape As Shape, ByVal startupMoney As Integer, ByRef board As Board)
        _shape = shape
        _money = startupMoney
        _boardPlayerOn = board
        _uniqueID = DateTime.UtcNow.GetHashCode
        _hasGetOutOfJailCard = False
        _currentlyInJail = False
        _position = 39 'go's location
        _die = New Die

        Me._property = New List(Of TheProperty)
        Me._listOfCompleteSets = New List(Of Boolean)
    End Sub

    Public Sub ChooseAction()
        Console.WriteLine("----------------------------------")
        Console.WriteLine("Press 1 to roll the die.")
        Console.WriteLine("Press 2 to take an action.")
        Dim ans As String = Console.ReadLine()
        While ans <> 1 And ans <> 2
            Console.WriteLine("Press 1 to roll the die.")
            Console.WriteLine("Press 2 to take an action.")
            ans = Console.ReadLine()
        End While
        If ans = 1 Then
            'roll die
            Me.MovePlayer(RollDie())
        Else
            'take action
            Console.WriteLine("Press 1 to view current property. ")
            Console.WriteLine("Press 2 to begin investing in property. ")
            Dim choice As String = Console.ReadLine()
            While choice <> 1 And choice <> 2
                Console.WriteLine("Press 1 to view current property. ")
                Console.WriteLine("Press 2 to begin investing in property. ")
                choice = Console.ReadLine()
            End While
            If choice = 1 Then
                Me.ViewProperty()
            Else
                Me.MakeInvestment()
            End If
        End If
    End Sub

    Public Sub ViewProperty()
        Console.WriteLine("Property owned: ")
        For Each prop In _property
            Console.WriteLine(prop._name & " : " & prop._color.ToString & " (" & prop.NumberOfHouses & ") houses (" & prop.NumberOfHotels & ") hotels. ")
        Next
    End Sub

    Private Function ReturnListOfCompleteSetBools() As List(Of Boolean)
        Return _listOfCompleteSets
    End Function

    Private Sub MakeInvestment()
        Dim props = _property
        Dim num As Integer = 1
        Dim positions As New List(Of Integer)
        Dim count As Integer = 0
        For Each prop In props
            If prop._partOfCompleteSet Then
                Console.WriteLine(num & " - " & prop._name & " : " & prop._color.ToString & ". ")
                Console.WriteLine("--- (" & prop.NumberOfHouses & ") houses and (" & prop.NumberOfHotels & ") hotels. ")
                Console.WriteLine("House price: $" & prop._housePrice)
                positions.Add(count)
                num += 1
            End If
            count += 1
        Next
        Dim numOfOptions = num - 1
        Console.WriteLine("")
        Console.WriteLine("Choose a property to upgrade by number.")
        Console.WriteLine("Press 0 to exit.")
        Console.WriteLine("/*--- You must distribute houses evenly among your properties. ")
        Console.WriteLine("After 4 houses on a single property, you may purchase a hotel. ---*/")
        Dim ans As String = Console.ReadLine()
        While ans < 0 Or ans > numOfOptions
            Console.WriteLine("Choose a property to upgrade by number.")
            Console.WriteLine("Press 0 to exit.")
            ans = Console.ReadLine()
        End While
        '   Check for existing investments
        If ans = 0 Then
            'exit
            Console.WriteLine("Exiting make an investment. ")
        Else
            Dim propertyToUpgrade As TheProperty = _property.Item(positions.Item(ans - 1))


            Dim colorGroupProperties As List(Of TheProperty) = GetAllPropertiesOfColorGroup(propertyToUpgrade._color)
            Dim allowable As Boolean = True
            For Each prop In colorGroupProperties
                'Check its allowed to build house here
                If propertyToUpgrade.NumberOfHouses > prop.NumberOfHouses Then
                    'not allowable upgrade
                    allowable = False
                End If

            Next
            If allowable Then
                If propertyToUpgrade.NumberOfHouses > 4 Then

                    Console.WriteLine("4 houses already here, you must  build hotel. ")
                    TryPurchaseHotel(propertyToUpgrade)
                Else
                    TryPurchaseHouse(propertyToUpgrade)
                End If
            Else
                'not allowable, try again or exit
                Console.WriteLine("Not allowed to build house at " & propertyToUpgrade._name & ". ")
            End If
            Console.WriteLine(" ")

        End If
        Console.WriteLine("Exiting make investment...")
    End Sub

    Public Function TryPurchaseHouse(ByRef theProperty As TheProperty) As Boolean
        Dim passed = False
        Dim costOfHouse As Integer = theProperty._housePrice
        If Me._money < costOfHouse Then
            Throw New Exception("Out of money in try purchase new house")
            Return False
        Else
            theProperty._houses.Add(New House)
            Me._money -= costOfHouse
            Console.WriteLine("Successful house purchase! ")
            Console.WriteLine(theProperty._name & " now has (" & theProperty._houses.Count & ") house(s). ")
            passed = True
        End If
        Return passed
    End Function

    Public Function TryPurchaseHotel(ByRef theProperty As TheProperty) As Boolean
        Dim passed As Boolean = False
        If Me._money < theProperty._houseRentPrices.Item(4) Then ' the fouth element is the hotel price
            'not enough money
            Return passed
        Else
            'purchase hotel
            Me._money -= theProperty._houseRentPrices.Item(4)
            theProperty._hotels.Add(New Hotel) 'TODO update
            passed = True
        End If
        Return passed
    End Function

    Public Function GetAllPropertiesOfColorGroup(ByVal color As TheProperty.ColorSet) As List(Of TheProperty)
        Dim toBeReturned As New List(Of TheProperty)
        For Each place In _property
            If place._color = color Then
                toBeReturned.Add(place)
            End If
        Next
        Return toBeReturned
    End Function

    Public Function RollDie() As Integer
        Dim die As New Die
        Return die.ThrowDice
    End Function

    Public Sub MovePlayer(ByVal dieCast As Integer)
        _position = _position + dieCast

        'If player off board adjust
        If _position > 39 Then
            _position = _position - _boardPlayerOn.BoardSize
            'passed go
            Me._money += 200
        ElseIf _position < 0 Then
            _position = 40 - _position
        End If

        Console.WriteLine("Player " & Me.Name & " moves " & dieCast & " spaces to position " & _position & ". ")
    End Sub

    Public Sub CheckForCompleteColorSets()
        'Iterate through property list to check for complete sets
        Dim brownCount As Integer = 0
        Dim lightBlueCount As Integer = 0
        Dim maroonCount As Integer = 0
        Dim orangeCount As Integer = 0
        Dim redCount As Integer = 0
        Dim yellowCount As Integer = 0
        Dim greenCount As Integer = 0
        Dim blueCount As Integer = 0

        For Each prop In _property
            If prop._color = TheProperty.Color.Brown Then
                brownCount += 1
                If brownCount = 2 Then
                    Me._completeBrown = True
                End If
            ElseIf prop._color = TheProperty.Color.LightBlue Then
                lightBlueCount += 1
                If lightBlueCount = 3 Then
                    Me._completeLightBlue = True
                End If
            ElseIf prop._color = TheProperty.Color.Maroon Then
                maroonCount += 1
                If maroonCount = 3 Then
                    Me._completeMaroon = True
                End If
            ElseIf prop._color = TheProperty.Color.Orange Then
                orangeCount += 1
                If orangeCount = 3 Then
                    Me._completeOrange = True
                End If
            ElseIf prop._color = TheProperty.Color.Red Then
                redCount += 1
                If redCount = 3 Then
                    Me._completeRed = True
                End If
            ElseIf prop._color = TheProperty.Color.Yellow Then
                yellowCount += 1
                If yellowCount = 3 Then
                    Me._completeYellow = True
                End If
            ElseIf prop._color = TheProperty.Color.Green Then
                greenCount += 1
                If greenCount = 3 Then
                    Me._completeGreen = True
                End If
            ElseIf prop._color = TheProperty.Color.Blue Then
                blueCount += 1
                If blueCount = 2 Then
                    Me._completeBlue = True
                End If
            End If
        Next

        For Each prop In _property
            If prop._color = TheProperty.Color.Brown Then
                If brownCount = 2 Then
                    prop._partOfCompleteSet = True
                End If
            ElseIf prop._color = TheProperty.Color.LightBlue Then
                If lightBlueCount = 3 Then
                    prop._partOfCompleteSet = True
                End If
            ElseIf prop._color = TheProperty.Color.Maroon Then
                If maroonCount = 3 Then
                    prop._partOfCompleteSet = True
                End If
            ElseIf prop._color = TheProperty.Color.Orange Then
                If orangeCount = 3 Then
                    prop._partOfCompleteSet = True
                End If
            ElseIf prop._color = TheProperty.Color.Red Then
                If redCount = 3 Then
                    prop._partOfCompleteSet = True
                End If
            ElseIf prop._color = TheProperty.Color.Yellow Then
                If yellowCount = 3 Then
                    prop._partOfCompleteSet = True
                End If
            ElseIf prop._color = TheProperty.Color.Green Then
                If greenCount = 3 Then
                    prop._partOfCompleteSet = True
                End If
            ElseIf prop._color = TheProperty.Color.Blue Then
                If blueCount = 2 Then
                    prop._partOfCompleteSet = True
                End If
            End If
        Next

        Me._listOfCompleteSets.Clear()
        Me._listOfCompleteSets.Add(_completeBrown)
        Me._listOfCompleteSets.Add(_completeLightBlue)
        Me._listOfCompleteSets.Add(_completeMaroon)
        Me._listOfCompleteSets.Add(_completeOrange)
        Me._listOfCompleteSets.Add(_completeRed)
        Me._listOfCompleteSets.Add(_completeYellow)
        Me._listOfCompleteSets.Add(_completeGreen)
        Me._listOfCompleteSets.Add(_completeBlue)

    End Sub

    'Public Function PayForProperty(ByRef prop As TheProperty) As Boolean
    '    Dim cost As Integer = prop.Price
    '    If _money >= cost Then
    '        '   Adjust balance
    '        _money = _money - cost
    '        '   Add to property list
    '        _property.Add(prop)
    '        prop._ownedBy = Me
    '        Return True
    '    Else
    '        Return False
    '    End If
    'End Function

    Public Function PayRent(ByRef toProperty As TheProperty) As Boolean
        'Check if part of complete set and adjust payment
        Dim priceToPay As Integer
        If toProperty._partOfCompleteSet Then
            priceToPay = toProperty._rent * 2
        Else
            priceToPay = toProperty._rent
        End If

        If priceToPay > Me._money Then
            'bankrupt
            'TODO
            Throw New Exception("not enough money to pay rent")
            Return False
        Else
            Me._money = Me._money - priceToPay
            toProperty._ownedBy._money = toProperty._ownedBy._money + priceToPay
            Console.WriteLine("Player " & Me._shape.ToString & " payed " & priceToPay & " as rent to owner " & toProperty._ownedBy.Name & ". ")
            Console.WriteLine("Player " & Me._shape.ToString & " still has " & Me._money & " left in bank. ")
        End If

        'TODO
        Return True
    End Function

    Public Function PayRent(ByRef toUtility As SpotUtility) As Boolean
        'Does the owner own one or two utilities
        Dim owner = toUtility._ownedBy
        Dim toPay As Integer
        If owner._utilities.Count = 2 Then
            'pay 10x die roll
            toPay = 10 * _die.ThrowDice()
        ElseIf owner._utilities.Count = 1 Then
            'pay 4x die roll
            toPay = 4 * _die.ThrowDice()
        Else
            'error
            Throw New Exception("pay rent to utility error with owner._utilities count")
            Return False
        End If
        If Me._money < toPay Then
            Throw New Exception("bankrupt when paying rent for utility")
        End If
        Me._money = Me._money - toPay
        owner._money = owner._money + toPay
        Return True
    End Function

    Public Function PayRent(ByRef toRailroad As SpotRailroad) As Boolean
        'Check how many railroads owned by owner
        Dim owner = toRailroad._ownedBy
        Dim toPay As Integer
        If owner._railroads.Count = 1 Then
            'Pay 25
            toPay = 25
        ElseIf owner._railroads.Count = 2 Then
            'pay 50
            toPay = 50
        ElseIf owner._railroads.Count = 3 Then
            toPay = 100
        ElseIf owner._railroads.Count = 4 Then
            toPay = 200
        Else
            Throw New Exception("error paying rent for railroads.count")
            Return False
        End If
        If Me._money < toPay Then
            Throw New Exception("bankrupt when paying rent for railroad")
            Return False
        End If
        Me._money = Me._money - toPay
        owner._money = owner._money + toPay
        Return True
    End Function

    Public Function TryPurchaseProperty(ByRef toProperty As TheProperty) As Boolean
        Dim passed As Boolean = False

        If ChooseToPayOrPass(toProperty) Then
            Dim priceOfProperty As Integer = toProperty.Price
            If priceOfProperty > Me._money Then
                'cancel
                'TODO
                Throw New Exception("no money left to purchase property.")
                Return False
            Else
                Me._money = Me._money - priceOfProperty
                toProperty.SetPropertyOwnership(Me)
                Me._property.Add(toProperty)
                Console.WriteLine("Player " & Me.Name & " now owns " & toProperty._name & ". ")
                ReadBank()
                passed = True
            End If
        End If
        Return passed
    End Function

    Public Function TryPurchaseRailroad(ByRef toRailroad As SpotRailroad) As Boolean
        If ChooseToPayOrPass(toRailroad) Then
            Dim priceOfRailroad As Integer = toRailroad.Price
            If priceOfRailroad > Me._money Then
                'cancel 
                'TODO'
                Return False
            Else
                Me._money = Me._money - priceOfRailroad
                toRailroad.SetRailroadOwnership(Me)
                Console.WriteLine("Player " & Me.Name & " now owns " & toRailroad._name & ". ")
                ReadBank()
                Return True
            End If
        End If
        Return True
    End Function

    Public Function TryPurchaseUtility(ByRef toUtility As SpotUtility) As Boolean
        If ChooseToPayOrPass(toUtility) Then
            Dim priceOfUtility As Integer = toUtility.Price
            If priceOfUtility > Me._money Then
                'cancel
                'TODO
                Return False
            Else
                Me._money = Me._money - priceOfUtility
                toUtility.SetUtilityOwnership(Me)
                Console.WriteLine("Player " & Me.Name & " now owns " & toUtility._name & ". ")
                ReadBank()
                Return True
            End If
        End If
        Return True
    End Function

    Public Function ChooseToPayOrPass(ByRef toProperty As TheProperty) As Boolean
        Console.WriteLine("You landed on " & toProperty._name & ". Would you like to own it?")
        Console.WriteLine("Cost - " & toProperty.Price & ". ")
        Console.WriteLine("Current bank - " & Me._money & ". ")
        Console.WriteLine("Choose 1 to purchase, 2 to pass. ") 'TODO setup bid war
        Dim answer As String = Console.ReadLine()
        While (answer <> "1" And answer <> "2")
            Console.WriteLine("Invalid choice.")
            Console.WriteLine("Choose 1 to purchase, 2 to pass. ")
            answer = Console.ReadLine()
        End While
        If answer = 1 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function ChooseToPayOrPass(ByRef toProperty As SpotRailroad) As Boolean
        Console.WriteLine("You landed on " & toProperty._name & ". Would you like to own it?")
        Console.WriteLine("Cost - " & toProperty.Price & ". ")
        Console.WriteLine("Current bank - " & Me._money & ". ")
        Console.WriteLine("Choose 1 to purchase, 2 to pass. ") 'TODO setup bid war
        Dim answer As String = Console.ReadLine()
        While (answer <> "1" And answer <> "2")
            Console.WriteLine("Invalid choice.")
            Console.WriteLine("Choose 1 to purchase, 2 to pass. ")
            answer = Console.ReadLine()
        End While
        If answer = 1 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function ChooseToPayOrPass(ByRef toProperty As SpotUtility) As Boolean
        Console.WriteLine("You landed on " & toProperty._name & ". Would you like to own it?")
        Console.WriteLine("Cost - " & toProperty.Price & ". ")
        Console.WriteLine("Current bank - " & Me._money & ". ")
        Console.WriteLine("Choose 1 to purchase, 2 to pass. ") 'TODO setup bid war
        Dim answer As String = Console.ReadLine()
        While (answer <> "1" And answer <> "2")
            Console.WriteLine("Invalid choice.")
            Console.WriteLine("Choose 1 to purchase, 2 to pass. ")
            answer = Console.ReadLine()
        End While
        If answer = 1 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Sub PayIncomeTax()
        'Make sure has enough to pay 200
        If Me._money < 200 Then
            Throw New Exception("Not enough money in bank to pay Income Tax.")
        End If

        Dim tenPercent As Integer = Me._money / 10

        If tenPercent > 200 Then
            'Pay 10%
            Me._money = Me._money - tenPercent
            Console.WriteLine("You payed $" & tenPercent & " as income tax. ")
        Else
            'Pay $200
            Me._money = Me._money - 200
            Console.WriteLine("You payed $200 as income tax. ")
        End If
        Console.WriteLine("Remaining balance is $" & Me._money & ". ")
    End Sub

    Public Sub PayLuxuryTax()
        If Me._money < 75 Then
            Throw New Exception("Not enough money in bank to pay Luxury Tax.")
        End If

        Me._money = Me._money - 75

        Console.WriteLine("You payed $75 as Luxury Tax.")
        Console.WriteLine("Remaining balance is $" & Me._money & ". ")
    End Sub

    Public Sub PickUpCommunityChestCard(ByRef card As CommunityChestCard)

        Console.WriteLine(card._instructionText)

        'Card logic
        Select Case card._id
            Case 0
                'Advance to go
                Me._position = _boardPlayerOn._places.Count - 1 'Go is at end of board
                Me._money = Me._money + 200 'Collect $200
            Case 1
                'Collect $200
                Me._money = Me._money + 200
            Case 2
                'Pay $50
                Me._money = Me._money - 50
            Case 3
                'Collect $50
                Me._money = Me._money + 50
            Case 4
                'Get out of jail card
                _hasGetOutOfJailCard = True
            Case 5
                'Go to jail
                _currentlyInJail = True
            Case 6
                'Collect $50 from every player
                For Each player In _boardPlayerOn._playersOnBoard
                    'if not me
                    If player._uniqueID <> Me._uniqueID Then
                        player._money = player._money - 50
                        Me._money = Me._money + 50
                    End If
                Next
            Case 7
                'Collect $100
                Me._money = Me._money + 100
            Case 8
                'Collect $20
                Me._money = Me._money + 20
            Case 9
                'Collect $10 from each player
                For Each player In _boardPlayerOn._playersOnBoard
                    'if not me
                    If player._uniqueID <> Me._uniqueID Then
                        player._money = player._money - 10
                        Me._money = Me._money + 10
                    End If
                Next
            Case 10
                'Collect $100
                Me._money = Me._money + 100
            Case 11
                'Pay $100
                Me._money = Me._money - 100
            Case 12
                'Pay $150
                Me._money = Me._money - 150
            Case 13
                'Collect $25
                Me._money = Me._money + 25
            Case 14
                'Pay $40 per house - $115 per hotel
                For Each prop In Me._property
                    'Pay $40 for house  
                    Console.WriteLine("At " & prop._name & "... ")
                    Dim costOfHouses = prop.NumberOfHouses * 40
                    Console.WriteLine("You payed $" & costOfHouses & " for your (" & prop.NumberOfHouses & ") houses. ")
                    Me._money -= costOfHouses
                    Dim costOfHotels = prop.NumberOfHotels * 115
                    Console.WriteLine("You payed $" & costOfHotels & " for your (" & prop.NumberOfHotels & ") hotels. ")
                    Me._money -= costOfHotels
                Next
                'gotem
            Case 15
                'Collect $10
                Me._money = Me._money + 10
            Case 16
                'Collect $100
                Me._money = Me._money + 100
            Case Else
                Console.WriteLine("Community chest card ERROR case statement")
        End Select
        ReadBank()
    End Sub

    Public Sub PickupChanceCard(ByRef card As ChanceCard)
        Console.WriteLine(card._instructionText)

        Select Case card._id
            Case 0
                'advance to go collect 200
                Me._position = _boardPlayerOn._places.Count - 1 'Go is at end of board
                Me._money = Me._money + 200 'Collect $200
            Case 1
                'advance to illinois, if pass go collect 200
                Dim fromPosition As Integer = Me._position
                Dim toPosition As Integer = _boardPlayerOn._locationByNameDictionary("Illinois Avenue")._boardLocation
                'Move
                Me._position = toPosition
                'Logic for passing go
                If fromPosition > toPosition Then
                    'pass go
                    Me._money = Me._money + 200
                End If
            Case 2
                'advance to st charles, if pass go collect 200
                Dim fromPosition As Integer = Me._position
                Dim toPosition As Integer = _boardPlayerOn._locationByNameDictionary("St. Charles Place")._boardLocation
                'Move
                Me._position = toPosition
                If fromPosition > toPosition Then
                    'pass go
                    Me._money = Me._money + 200
                End If
            Case 3
                'advance to utility
                Dim fromPosition As Integer = Me._position
                If fromPosition >= _boardPlayerOn._locationByNameDictionary("Electric Company")._boardLocation And fromPosition < _boardPlayerOn._locationByNameDictionary("Water Works")._boardLocation Then
                    'Move to water works
                    Me._position = _boardPlayerOn._locationByNameDictionary("Water Works")._boardLocation
                Else
                    Me._position = _boardPlayerOn._locationByNameDictionary("Electric Company")._boardLocation
                    If fromPosition >= _boardPlayerOn._locationByNameDictionary("Water Works")._boardLocation Then
                        'also pass go
                        Me._money = Me._money + 200
                    End If
                End If
            Case 4
                'advance to railroad
                Me.AdvanceToNearestRailroad()
            Case 5
                'advance to railroad
                Me.AdvanceToNearestRailroad()
            Case 6
                'collect 50
                Me._money = Me._money + 50
            Case 7
                'get out of jail free card
                Me._hasGetOutOfJailCard = True
            Case 8
                'go back 3 spaces
                Me.MovePlayer(-3)
            Case 9
                'go to jail
                Me._currentlyInJail = True
            Case 10
                'pay for each house/hotel
                'Pay $25 per house - $100 per hotel

                For Each prop In Me._property
                    Dim costOfHouses = prop.NumberOfHouses * 25
                    Me._money -= costOfHouses
                    Console.WriteLine("You payed " & costOfHouses & " for your (" & prop.NumberOfHouses & ") houses. ")
                    Dim costofHotels = prop.NumberOfHotels * 100
                    Me._money -= costofHotels
                    Console.WriteLine("You payed " & costofHotels & " for your (" & prop.NumberOfHotels & ") hotels. ")
                Next

                'gotem
            Case 11
                'pay 15
                Me._money = Me._money - 15
            Case 12
                'go to reading railroad
                Dim fromPosition = Me._position
                Me._position = _boardPlayerOn._locationByNameDictionary("Reading Railroad")._boardLocation
                If fromPosition > _boardPlayerOn._locationByNameDictionary("Reading Railroad")._boardLocation Then
                    'pass go
                    Me._money = Me._money + 200
                End If
            Case 13
                'go to boardwalk
                Dim fromPosition = Me._position
                Me._position = _boardPlayerOn._locationByNameDictionary("Boardwalk")._boardLocation
                If fromPosition <= _boardPlayerOn._locationByNameDictionary("Boardwalk")._boardLocation Then
                    'pass go
                    Me._money = Me._money + 200
                End If
            Case 14
                'pay each player 50 
                For Each player In _boardPlayerOn._playersOnBoard
                    'if not me
                    If player._uniqueID <> Me._uniqueID Then
                        player._money = player._money + 50
                        Me._money = Me._money - 50
                    End If
                Next
            Case 15
                'collect 150
                Me._money = Me._money + 150
            Case 16
                'collect 100
                Me._money = Me._money + 100
        End Select
        ReadBank()
    End Sub

    Private Sub AdvanceTo(ByVal placeName As String)
        Dim fromPosition As Integer = Me._position
        Dim toPosition As Integer = _boardPlayerOn._locationByNameDictionary(placeName)._boardLocation
        'move
        Me._position = toPosition
        If fromPosition > toPosition Then
            'pass go
            Me._money = Me._money + 200
        End If
    End Sub

    Private Sub AdvanceToNearestRailroad()
        'advance to railroad
        Dim fromPosition As Integer = Me._position
        If fromPosition < _boardPlayerOn._locationByNameDictionary("Reading Railroad")._boardLocation Then
            'advance to reading railroad
            Me._position = _boardPlayerOn._locationByNameDictionary("Reading Railroad")._boardLocation
        ElseIf fromPosition < _boardPlayerOn._locationByNameDictionary("Pennsylvania Railroad")._boardLocation And fromPosition >= _boardPlayerOn._locationByNameDictionary("Reading Railroad")._boardLocation Then
            'advance to penn railroad
            Me._position = _boardPlayerOn._locationByNameDictionary("Pennsylvania Railroad")._boardLocation
        ElseIf fromPosition < _boardPlayerOn._locationByNameDictionary("B&O Railroad")._boardLocation And fromPosition >= _boardPlayerOn._locationByNameDictionary("Pennsylvania Railroad")._boardLocation Then
            'advance to bo railroad
            Me._position = _boardPlayerOn._locationByNameDictionary("B&O Railroad")._boardLocation
        ElseIf fromPosition < _boardPlayerOn._locationByNameDictionary("Short Line")._boardLocation And fromPosition >= _boardPlayerOn._locationByNameDictionary("B&O Railroad")._boardLocation Then
            'advance to short line
            Me._position = _boardPlayerOn._locationByNameDictionary("Short Line")._boardLocation
        Else
            'advance to reading railroad and pass go
            Me._position = _boardPlayerOn._locationByNameDictionary("Reading Railroad")._boardLocation
            Me._money = Me._money + 200
        End If
    End Sub

    Public Sub ReadBank()
        Console.WriteLine(Me.Name & " has $" & Me._money & " left in bank. ")
    End Sub

#Region "LandedOns"
    Public Function LandedOnProperty() As Boolean
        If _boardPlayerOn.IsPropertyAt(_position) Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function LandedOnCommunityChest() As Boolean
        If _boardPlayerOn.IsCommunityChestAt(_position) Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function LandedOnTax() As Boolean
        If _boardPlayerOn.IsTaxAt(_position) Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function LandedOnRailroad() As Boolean
        If _boardPlayerOn.IsRailroadAt(_position) Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function LandedOnChanceCard() As Boolean
        If _boardPlayerOn.IsChanceCardAt(_position) Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function LandedOnBlank() As Boolean
        If _boardPlayerOn.IsBlankAt(_position) Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function LandedOnUtility() As Boolean
        If _boardPlayerOn.IsUtilityAt(_position) Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region

#Region "Properties"
    Public ReadOnly Property PlayerPosition
        Get
            Return _position
        End Get
    End Property

    Public ReadOnly Property Name
        Get
            Return _shape.ToString
        End Get
    End Property
#End Region

    'Public Sub getPayed(...)
    'Public Sub throwDice(...)
End Class
