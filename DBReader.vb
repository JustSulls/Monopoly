﻿Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class DBReader
    Private conn As SqlConnection
    Private reader As SqlDataReader
    Private sqlCmd As SqlCommand

    Public Sub New()
        conn = New SqlConnection With {
            .ConnectionString = "Server=COMPY\SQLEXPRESS; Database=Monopoly; Integrated Security=True"
        }
        sqlCmd = New SqlCommand With {
            .CommandText = "SELECT * FROM [Monopoly].[dbo].[Properties]",
            .Connection = conn
        }

        conn.Open()

        reader = sqlCmd.ExecuteReader()
    End Sub

    Public Sub New(ByVal queryStr As String)
        conn = New SqlConnection With {
            .ConnectionString = "Server=COMPY\SQLEXPRESS; Database=Monopoly; Integrated Security=True"
        }
        sqlCmd = New SqlCommand With {
            .CommandText = queryStr,
            .Connection = conn
        }

        conn.Open()

        reader = sqlCmd.ExecuteReader()
    End Sub

    Public Sub New(ByVal connectionStr As String, ByVal queryStr As String)
        conn = New SqlConnection With {
            .ConnectionString = connectionStr
        }
        sqlCmd = New SqlCommand With {
            .CommandText = queryStr,
            .Connection = conn
        }

        conn.Open()

        reader = sqlCmd.ExecuteReader()
    End Sub

    Public Function GetDBReader() As SqlDataReader
        Return Me.reader
    End Function
End Class
