﻿Imports System.Data.SqlClient

Public Class Board
    Public _places As List(Of Place)
    Public _chanceCardDeck As List(Of ChanceCard)
    Public _communityChestDeck As List(Of CommunityChestCard)
    Public _playersOnBoard As List(Of Player)
    Public _locationByNameDictionary As Dictionary(Of String, Place)
    Public _DBreader As DBReader


    Public Sub New()
        Me._places = New List(Of Place)
        Me._chanceCardDeck = New List(Of ChanceCard)
        Me._communityChestDeck = New List(Of CommunityChestCard)
        Me._playersOnBoard = New List(Of Player)
        _DBreader = New DBReader()

        Me._locationByNameDictionary = New Dictionary(Of String, Place)
    End Sub

#Region "CheckSpotType"
    Public Function IsPropertyAt(ByVal boardPosition) As Boolean
        If _places.Item(boardPosition).GetType() Is GetType(TheProperty) Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function IsCommunityChestAt(ByVal boardPosition) As Boolean
        If _places.Item(boardPosition).GetType() Is GetType(SpotCommunityChest) Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function IsTaxAt(ByVal boardPosition) As Boolean
        If _places.Item(boardPosition).GetType() Is GetType(SpotTax) Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function IsRailroadAt(ByVal boardPosition) As Boolean
        If _places.Item(boardPosition).GetType() Is GetType(SpotRailroad) Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function IsChanceCardAt(ByVal boardPosition) As Boolean
        If _places.Item(boardPosition).GetType() Is GetType(SpotChanceCard) Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function IsBlankAt(ByVal boardPosition) As Boolean
        If _places.Item(boardPosition).GetType() Is GetType(SpotBlank) Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function IsUtilityAt(ByVal boardPosition) As Boolean
        If _places.Item(boardPosition).GetType() Is GetType(SpotUtility) Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region

#Region "ReturnSpotOnBoard"
    Public Function SpotAt(ByVal playerPosition As Integer) As Place
        Return _places.Item(playerPosition)
    End Function

    Public Function PropertyAt(ByVal playerPosition As Integer) As TheProperty
        Return _places.Item(playerPosition)
    End Function

    Public Function CommunityChestSpotAt(ByVal playerPosition As Integer) As SpotCommunityChest
        Return _places.Item(playerPosition)
    End Function

    Public Function TaxAt(ByVal playerPosition As Integer) As SpotTax
        Return _places.Item(playerPosition)
    End Function

    Public Function RailroadAt(ByVal playerPosition As Integer) As SpotRailroad
        Return _places.Item(playerPosition)
    End Function

    Public Function ChanceCardSpotAt(ByVal playerPosition As Integer) As SpotChanceCard
        Return _places.Item(playerPosition)
    End Function

    Public Function UtilityAt(ByVal playerPosition As Integer) As SpotUtility
        Return _places.Item(playerPosition)
    End Function
#End Region

    Private Function GetPropertyColor(ByVal Color As String) As TheProperty.ColorSet
        Dim toReturn As TheProperty.ColorSet
        If Color = "Brown" Then
            toReturn = TheProperty.Color.Brown
        ElseIf Color = "Light Blue" Then
            toReturn = TheProperty.Color.LightBlue
        ElseIf Color = "Maroon" Then
            toReturn = TheProperty.Color.Maroon
        ElseIf Color = "Orange" Then
            toReturn = TheProperty.Color.Orange
        ElseIf Color = "Red" Then
            toReturn = TheProperty.Color.Red
        ElseIf Color = "Yellow" Then
            toReturn = TheProperty.Color.Yellow
        ElseIf Color = "Green" Then
            toReturn = TheProperty.Color.Green
        ElseIf Color = "Blue" Then
            toReturn = TheProperty.Color.Blue
        Else
            toReturn = Nothing
            Throw New Exception("Error during color assignment.")
        End If
        Return toReturn
    End Function

    Public Sub GenerateBoard()
        Dim reader As SqlDataReader = _DBreader.GetDBReader()

        While reader.HasRows
            While reader.Read()
                Dim name As String = reader.GetString(1)
                Dim color As String = reader.GetString(2)
                Dim price As Integer = reader.GetInt32(3)
                Dim MortgageVal As Integer = reader.GetInt32(4)
                Dim BuildingCost As Integer = reader.GetInt32(5)
                Dim Rent As Integer = reader.GetInt32(6)
                Dim Rent1House As Integer = reader.GetInt32(7)
                Dim rent2House As Integer = reader.GetInt32(8)
                Dim rent3House As Integer = reader.GetInt32(9)
                Dim rent4House As Integer = reader.GetInt32(10)
                Dim rentHotel As Integer = reader.GetInt32(11)
                Dim boardLocation As Integer = reader.GetInt32(12)

                _places.Add(New TheProperty(name,
                                            GetPropertyColor(color),
                                            price, MortgageVal,
                                            BuildingCost,
                                            Rent,
                                            Rent1House,
                                            rent2House,
                                            rent3House,
                                            rent4House,
                                            rentHotel,
                                            boardLocation))
            End While
            reader.NextResult()
        End While
        _places.Add(New SpotCommunityChest("Community Chest 1", 1))
        _places.Add(New SpotTax("Income Tax", SpotTax.Tax.IncomeTax, 3))
        _places.Add(New SpotRailroad("Reading Railroad", 200, 4))
        _places.Add(New SpotChanceCard("Chance 1", 6))
        _places.Add(New SpotBlank("Just Visiting", 9))
        _places.Add(New SpotUtility("Electric Company", 150, 11))
        _places.Add(New SpotRailroad("Pennsylvania Railroad", 200, 14))
        _places.Add(New SpotCommunityChest("Community Chest 2", 16))
        _places.Add(New SpotBlank("Free Parking", 19))
        _places.Add(New SpotChanceCard("Chance 2", 21))
        _places.Add(New SpotRailroad("B&O Railroad", 200, 24))
        _places.Add(New SpotUtility("Water Works", 150, 27))
        _places.Add(New SpotGoToJail("Go to Jail", 29))
        _places.Add(New SpotCommunityChest("Community Chest 3", 32))
        _places.Add(New SpotRailroad("Short Line", 200, 34))
        _places.Add(New SpotChanceCard("Chance 3", 35))
        _places.Add(New SpotTax("Luxury Tax", SpotTax.Tax.LuxuryTax, 37))
        _places.Add(New SpotGo("Go", 39))

        'Keep dictionary of place locations by name
        For Each place In _places
            _locationByNameDictionary.Add(place._name, place)
        Next
    End Sub



    Public ReadOnly Property BoardSize As Integer
        Get
            Return Me._places.Count
        End Get
    End Property
End Class
