﻿Public Class Monopoly
    Const STARTUP_MONEY As Integer = 1500
    Public _players As List(Of Player)
    Public Property Board As Board
    Public _communityChestDeck As DeckCommunityChest
    Public _chanceCardDeck As DeckChance

    Public Sub New()
        _players = New List(Of Player)
        Board = New Board()
        _communityChestDeck = New DeckCommunityChest
        _chanceCardDeck = New DeckChance
    End Sub

    Public Sub StartGame(ByVal numberOfPlayers As Integer)
        Board.GenerateBoard()
        Setup(numberOfPlayers)

        Dim gameOver As Boolean = False
        While Not gameOver
            For Each player In _players

                'player.TryPurchaseProperty(_board._places(0))
                'player.TryPurchaseProperty(_board._places(2))
                'player.CheckForCompleteColorSets()
                player.ChooseAction()

                If player.LandedOnProperty() Then
                    '   If property owned, pay owner
                    If Board.PropertyAt(player.PlayerPosition).IsOwned Then
                        If Board.PropertyAt(player.PlayerPosition).Owner IsNot Me Then
                            If player.PayRent(Board.PropertyAt(player.PlayerPosition)) Then Console.WriteLine("Successfully payed rent")
                        Else
                            Console.WriteLine("Just visiting " & Board.PropertyAt(player.PlayerPosition)._name)
                        End If
                    Else
                        '   Else purchase property?
                        player.TryPurchaseProperty(Board.PropertyAt(player.PlayerPosition))
                        '   After potential buy check if set completed
                        player.CheckForCompleteColorSets()
                    End If
                ElseIf player.LandedOnCommunityChest() Then
                    Dim theCard = Board.CommunityChestSpotAt(player.PlayerPosition).TopDeckChanceCard(_communityChestDeck)
                    'Pick up community card
                    player.PickUpCommunityChestCard(theCard)
                ElseIf player.LandedOnTax() Then
                    'pay tax
                    If Board.TaxAt(player.PlayerPosition)._type = SpotTax.Tax.IncomeTax Then
                        player.PayIncomeTax()
                    Else
                        player.PayLuxuryTax()
                    End If
                ElseIf player.LandedOnRailroad() Then
                    'if property owned, pay owner
                    If Board.RailroadAt(player.PlayerPosition).IsOwned Then
                        If Board.RailroadAt(player.PlayerPosition)._ownedBy IsNot Me Then
                            player.PayRent(Board.RailroadAt(player.PlayerPosition))
                        End If
                    Else
                        'else choose purchase railroad
                        player.TryPurchaseRailroad(Board.RailroadAt(player.PlayerPosition))
                    End If
                ElseIf player.LandedOnChanceCard() Then
                    'pickup chance card
                    Dim theCard = Board.ChanceCardSpotAt(player.PlayerPosition).TopDeckChanceCard(_chanceCardDeck)
                    player.PickupChanceCard(theCard)
                ElseIf player.LandedOnUtility() Then
                    'If owned, pay appropriately
                    If Board.UtilityAt(player.PlayerPosition).IsOwned Then
                        If Board.UtilityAt(player.PlayerPosition)._ownedBy IsNot Me Then
                            player.PayRent(Board.UtilityAt(player.PlayerPosition))
                        End If
                    Else
                        'choose to buy
                        player.TryPurchaseUtility(Board.UtilityAt(player.PlayerPosition))
                    End If
                ElseIf player.LandedOnBlank() Then
                    'do nothing
                End If
            Next
        End While
    End Sub

    Private Function ChooseShape() As Player.Shape
        'Check existing player's shapes and adjust answers accordingly
        '...
        Console.WriteLine("Choose your player. ")
        Console.WriteLine("1: MazdaRX8")
        Console.WriteLine("2: MitsubishiLancer")
        Console.WriteLine("3: SubaruWRX")
        Dim answer As String = Console.ReadLine()
        While (answer <> "1" And answer <> "2" And answer <> "3")
            Console.WriteLine("Invalid choice.")
            Console.WriteLine("Choose your player. ")
            Console.WriteLine("1: MazdaRX8")
            Console.WriteLine("2: MitsubishiLancer")
            Console.WriteLine("3: SubaruWRX")
            answer = Console.ReadLine()
        End While
        Return answer - 1
    End Function
    Private Function Setup(ByVal numberofplayers As Integer) As List(Of Player)
        Dim players As New List(Of Player)

        For i As Integer = 0 To numberofplayers - 1
            Dim shape As Player.Shape = ChooseShape()
            '   Add player to list of players
            players.Add(New Player(shape, STARTUP_MONEY, Board))
            '   Add player to local player list
            Board._playersOnBoard.Add(_players.Item(i))
        Next

    End Function

End Class
