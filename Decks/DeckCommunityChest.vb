﻿Imports System.Data.SqlClient

Public Class DeckCommunityChest
    Public _theDeck As List(Of CommunityChestCard)
    Public _DBreader As DBReader

    Public Sub New()

        Dim queryStr As String = "SELECT * FROM [Monopoly].[dbo].[CommunityChestCards]"
        Me._DBreader = New DBReader(queryStr)
        Dim reader As SqlDataReader = _DBreader.GetDBReader()
        _theDeck = New List(Of CommunityChestCard)

        While reader.HasRows
            While reader.Read()
                Dim id As Integer = reader.GetInt32(0)
                Dim instructtxt As String = reader.GetString(1)
                _theDeck.Add(New CommunityChestCard(id, instructtxt))
            End While
            reader.NextResult()
        End While
    End Sub



End Class
