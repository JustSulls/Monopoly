﻿Imports System.Data.SqlClient

Public Class DeckChance
    Public _theDeck As List(Of ChanceCard)
    Public _DBreader As DBReader

    Public Sub New()
        Dim queryStr As String = "SELECT * FROM [Monopoly].[dbo].[ChanceCards]"
        Me._DBreader = New DBReader(queryStr)
        Dim reader As SqlDataReader = _DBreader.GetDBReader()
        _theDeck = New List(Of ChanceCard)

        While reader.HasRows
            While reader.Read()
                Dim id As Integer = reader.GetInt32(0)
                Dim instructtxt As String = reader.GetString(1)
                _theDeck.Add(New ChanceCard(id, instructtxt))
            End While
            reader.NextResult()
        End While
    End Sub
End Class
